<?php
	include 'Utilidades.php';
	$Conection = ConectaBD();
	
	$strSELECT = "SELECT id_alimento, nome_alimento FROM alimentos ORDER BY id_alimento";
	
	$r = $Conection->query($strSELECT);
	
	$retorno = array();
	
	if($r === false)
	{
		trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
	}
	else
	{
		while($alimento = $r->fetch_object())
		{
			array_push($retorno, utf8_encode($alimento->nome_alimento));
		}
	}
	
	$identificador = new stdClass();
	
	$identificador->alimentos = $retorno;
	
	echo json_encode($identificador);
?>