<?php
	include 'Utilidades.php';
		$Conection = ConectaBD();
		
		$strSELECT = "SELECT id_exercicio, nome_exercicio FROM exercicios ORDER BY id_exercicio";
		
		$r = $Conection->query($strSELECT);
		$retorno = array();
		
		if($r === false)
		{
			trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			while($exercicio = $r->fetch_object())
			{
				array_push($retorno, $exercicio->nome_exercicio);
			}
		}
		
		
		
		$strSELECT = "SELECT * FROM alimentos ORDER BY id_alimento";
		
		$r2 = $Conection->query($strSELECT);
		$retorno2 = array();
		
		if($r2 === false)
		{
			trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			while($alimento = $r2->fetch_object())
			{
				$objetoAlimento = new stdClass();
				$objetoAlimento->idAlimento = $alimento->id_alimento;
				$objetoAlimento->nomeAlimento = utf8_encode($alimento->nome_alimento);
				$objetoAlimento->porcao = $alimento->porcao;
				$objetoAlimento->calorias = $alimento->calorias;
				$objetoAlimento->carboidratos = $alimento->carboidratos;
				$objetoAlimento->proteinas = $alimento->proteinas;
				$objetoAlimento->gordurasTotais = $alimento->gorduras_totais;
				$objetoAlimento->gordurasSaturadas = $alimento->gorduras_saturadas;
				$objetoAlimento->gordurasTrans = $alimento->gorduras_trans;
				$objetoAlimento->fibraAlimentar = $alimento->fibra_alimentar;
				$objetoAlimento->sodio = $alimento->sodio;
				$objetoAlimento->tipoAlimento = utf8_encode($alimento->tipo_alimento);
				
								
				array_push($retorno2, $objetoAlimento);
				
			}
		}
		
		$strSELECT = "SELECT * FROM musculos ORDER BY id_musculo";
		
		$r3 = $Conection->query($strSELECT);
		$retorno3 = array();
		
		if($r3 === false)
		{
			trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			while($musculo = $r3->fetch_object())
			{
				$objetoMusculo = new stdClass();
				$objetoMusculo->idMusculo = $musculo->id_musculo;
				$objetoMusculo->nomeMusculo = utf8_encode($musculo->nome_musculo);
								
								
				array_push($retorno3, $objetoMusculo);
				
			}
		}
		
		$identificador = new stdClass();
		$identificador->exercicios = $retorno;
		$identificador->alimentos = $retorno2;
		$identificador->musculos = $retorno3;
		$jsonM = json_encode($identificador);
		
				
		echo $jsonM;
?>