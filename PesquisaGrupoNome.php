<?php
	if($_GET['nomeGrupo'])
	{
		include 'Utilidades.php';
		
		$nomeGP = $_GET['nomeGrupo'];
		
		$Conection = ConectaBD();

		$strQuery = "SELECT * FROM grupos WHERE nome_grupo LIKE '%$nomeGP%'";
		
		$resultadoQuery = $Conection->query($strQuery);
		
		if($resultadoQuery === false)
			{
				trigger_error('Wrong SQL: ' . $strQuery . ' Error: ' . $Conection->error, E_USER_ERROR);
			}
			else
			{
				$grupos = array();
				while($resultado = $resultadoQuery->fetch_object())
				{
					$infoGrupo = new stdClass();
					
					$infoGrupo->idGrupo = $resultado->id_grupo;
					$infoGrupo->nomeGrupo = utf8_encode($resultado->nome_grupo);
					$admJSON = ProcuraUsuarioGeral($resultado->fk_adm);
					$infoAdm = json_decode($admJSON);
					$infoGrupo->admGrupo = utf8_encode($infoAdm->nome);				
					$infoGrupo->descricaoGrupo = utf8_encode($resultado->descricao_grupo);
					
					array_push($grupos, $infoGrupo);
				}
											
				$jsonResultado = json_encode($grupos);
				
				echo $jsonResultado;
			}
			
	}
?>