<?php

	include 'Utilidades.php';
	if(!empty($_POST['dadosNovasMedidas']))
	{
		$Conection = ConectaBD();
		
		// Recebendo e decodificando o JSON com dados do usuario
		$strJSON = $_POST['dadosNovasMedidas'];
		//$strJSON = '{"id":"10","PesoAtual":"50","PesoObjetivo":"45","bPeso":"1","ToraxAtual":"-1","ToraxObjetivo":"-1","bTorax":"0","BracoAtual":"-1","BracoObjetivo":"-1","bBraco":"0", "CinturaAtual":"50","CinturaObjetivo":"45","bCintura":"1","CostasAtual":"-1","CostasObjetivo":"-1","bCostas":"0","CoxasAtual":"-1","CoxasObjetivo":"-1","bCoxas":"0", "PanturrilhaAtual":"-1", "PanturrilhaObjetivo":"-1", "bPanturrilha":"0"}';
		$dados = json_decode($strJSON);
		
		// Passando os dados para suas variaveis
		$id = $dados->id;
		
		$pesoAtual = $dados->PesoAtual;
		$pesoObjetivo = $dados->PesoObjetivo;
		$b_peso = $dados->bPeso;
		
		$toraxAtual = $dados->ToraxAtual;
		$toraxObjetivo = $dados->ToraxObjetivo;
		$b_torax = $dados->bTorax;
		
		$bracoAtual = $dados->BracoAtual;
		$bracoObjetivo = $dados->BracoObjetivo;
		$b_braco = $dados->bBraco;
		
		$cinturaAtual = $dados->CinturaAtual;
		$cinturaObjetivo = $dados->CinturaObjetivo;
		$b_cintura = $dados->bCintura;
		
		$costasAtual = $dados->CostasAtual;
		$costasObjetivo = $dados->CostasObjetivo;
		$b_costas = $dados->bCostas;
		
		$coxasAtual = $dados->CoxasAtual;
		$coxasObjetivo = $dados->CoxasObjetivo;
		$b_coxas = $dados->bCoxas;
		
		$panturrilhaAtual = $dados->PanturrilhaAtual;
		$panturrilhaObjetivo = $dados->PanturrilhaObjetivo;
		$b_panturrilha = $dados->bPanturrilha;
		
		// montando a query a ser executada
		$strUPDATE = "UPDATE usuarios SET peso_atual = '$pesoAtual', peso_objetivo = '$pesoObjetivo', b_peso = '$b_peso',
		torax_atual = '$toraxAtual', torax_objetivo = '$toraxObjetivo', b_torax = '$b_torax',
		braco_atual = '$bracoAtual', braco_objetivo = '$bracoObjetivo', b_braco = '$b_braco',
		cintura_atual = '$cinturaAtual', cintura_objetivo = '$cinturaObjetivo', b_cintura = '$b_cintura',
		costas_atual = '$costasAtual', costas_objetivo = '$costasObjetivo', b_costas = '$b_costas',
		coxas_atual = '$coxasAtual', coxas_objetivo = '$coxasObjetivo', b_coxas = '$b_coxas',
		panturrilha_atual = '$panturrilhaAtual', panturrilha_objetivo = '$panturrilhaObjetivo', b_panturrilha = '$b_panturrilha' WHERE id_usuario = '$id'";
		
		
			
		
		// atribuindo o valor da query a uma variavel para auxilio
		$resultadoQuery = $Conection->query($strUPDATE);
		
		if($resultadoQuery === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strUPDATE . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{			
			$strSELECT = "SELECT * FROM usuarios WHERE id_usuario = '$id'";
		
			$resultadoSELECT = $Conection->query($strSELECT);
			
			$vetor = new stdClass();
				
			$vetor->status = 2;
			
			if($resultadoSELECT === false) // testa se a query deu certo
			{
				trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
			}
			else
			{
				$linha = $resultadoSELECT->fetch_object();
				
				$vetor->status = 1;
				$vetor->pesoAtual = $linha->peso_atual;
				$vetor->pesoObjetivo = $linha->peso_objetivo;
				if($linha->b_peso == 0)
				{
					$vetor->peso = false;
				}
				else
				{
					$vetor->peso = true;
				}
				
				$vetor->toraxAtual = $linha->torax_atual;
				$vetor->toraxObjetivo = $linha->torax_objetivo;
				if($linha->b_torax == 0)
				{
					$vetor->torax = false;
				}
				else
				{
					$vetor->torax = true;
				}
				
				$vetor->bracoAtual = $linha->braco_atual;
				$vetor->bracoObjetivo = $linha->braco_objetivo;
				if($linha->b_braco == 0)
				{
					$vetor->braco = false;
				}
				else
				{
					$vetor->braco = true;
				}
				
				$vetor->cinturaAtual = $linha->cintura_atual;
				$vetor->cinturaObjetivo = $linha->cintura_objetivo;
				if($linha->b_cintura == 0)
				{
					$vetor->cintura = false;
				}
				else
				{
					$vetor->cintura = true;
				}
				
				$vetor->costasAtual = $linha->costas_atual;
				$vetor->costasObjetivo = $linha->costas_objetivo;
				if($linha->b_costas == 0)
				{
					$vetor->costas = false;
				}
				else
				{
					$vetor->costas = true;
				}
				
				$vetor->coxasAtual = $linha->coxas_atual;
				$vetor->coxasObjetivo = $linha->coxas_objetivo;
				if($linha->b_coxas == 0)
				{
					$vetor->coxas = false;
				}
				else
				{
					$vetor->coxas = true;
				}
				
				$vetor->panturrilhaAtual = $linha->panturrilha_atual;
				$vetor->panturrilhaObjetivo = $linha->panturrilha_objetivo;
				if($linha->b_panturrilha == 0)
				{
					$vetor->panturrilha = false;
				}
				else
				{
					$vetor->panturrilha = true;
				}
			}
			
			echo json_encode($vetor);
		}
	}

?>