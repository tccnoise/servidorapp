<?php
	include 'Utilidades.php';
	if(!empty($_POST['dadosNovoGrupo']))
	{
		$Conection = ConectaBD();
		
		$resposta = new stdClass();
		$resposta->status = 2;
		// Recebendo e decodificando o JSON com dados do usuario
		$strJSON = $_POST['dadosNovoGrupo'];
		//$strJSON = '{"idAdm":"11", "nomeGrupo":"Grupo Gyi2", "descGrupo":"poskposkposkpsoks"}';
		$dados = json_decode($strJSON);
		
		// Passando os dados para suas variaveis
		$idAdm = $dados->idAdm;
		$nomeGrupo = $dados->nomeGrupo;
		$descGrupo = $dados->descGrupo;
		
		// montando a query a ser executada
		$strINSERT = "INSERT INTO grupos (id_grupo, fk_adm, nome_grupo, descricao_grupo)
		VALUES ('', '$idAdm', '$nomeGrupo', '$descGrupo')";
		
		if($Conection->query($strINSERT) === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strINSERT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			$strSelect = "SELECT id_grupo FROM grupos WHERE fk_adm = '$idAdm' ORDER BY id_grupo";
			
			$resultadoId = $Conection->query($strSelect);
			if($resultadoId === false) // testa se a query deu certo
			{
				trigger_error('Wrong SQL: ' . $strSelect . ' Error: ' . $Conection->error, E_USER_ERROR);
			}
			else
			{
				$ultimoId = -1;
				
				while($linha = $resultadoId->fetch_row())
				{
					$ultimoId = $linha[0];
				}
				
				// montando a query a ser executada
				$strINSERT = "INSERT INTO grupos_usuarios (id_grupos_usuarios, fk_grupo, fk_usuario)
				VALUES ('', '$ultimoId', '$idAdm')";
				
				if($Conection->query($strINSERT) === false) // testa se a query deu certo
				{
					trigger_error('Wrong SQL: ' . $strINSERT . ' Error: ' . $Conection->error, E_USER_ERROR);
				}
				else
				{
					$resposta->status = 1;
					$resposta->idGrupo = $ultimoId;
				}					
			}
		}
		
		echo json_encode($resposta);
	}
?>