 <?php
	if(!empty($_GET['GruposUsuario']))
	{
		include 'Utilidades.php';
		
		$idUsuario = $_GET['GruposUsuario'];
		//$strJSON = '{"id":"11"}';
			
		$Conection = ConectaBD();
		
		//SELECT DISTINCT usuarios.nome, usuarios.sobrenome, usuarios.pontos, grupos.nome_grupo, grupos.descricao_grupo FROM usuarios INNER JOIN grupos_usuarios ON grupos_usuarios.fk_usuario = usuarios.id_usuario INNER JOIN grupos ON grupos_usuarios.fk_grupo = grupos.id_grupo WHERE id_grupo = '1'
		
		
		$strQueryIdGrupo = "SELECT fk_grupo FROM grupos_usuarios WHERE fk_usuario = '$idUsuario'";
		
		$resultadoQueryIdGrupo = $Conection->query($strQueryIdGrupo);
		
		if($resultadoQueryIdGrupo === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strQueryIdGrupo . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			/*$grupos = new stdClass();
			$grupos->idGrupo = array();
			$grupos->nomeGrupo = array();
			$grupos->admGrupo = array();
			$grupos->descGrupo = array();
			$grupos->usuarios = array();*/
			
			$grupos = array();
			
			$gruposId = array();
			
			while($linha = $resultadoQueryIdGrupo->fetch_row())
			{
				array_push($gruposId, $linha[0]);		
			}
			
			for($i = 0; $i < count($gruposId); $i++)
			{
				$idGrupo = $gruposId[$i];
				$strQueryGrupo = "SELECT DISTINCT usuarios.id_usuario, usuarios.nome, usuarios.sobrenome, usuarios.pontos, grupos.nome_grupo, grupos.descricao_grupo, grupos.fk_adm FROM usuarios INNER JOIN grupos_usuarios ON grupos_usuarios.fk_usuario = usuarios.id_usuario INNER JOIN grupos ON grupos_usuarios.fk_grupo = grupos.id_grupo WHERE id_grupo = '$idGrupo' ORDER BY usuarios.pontos DESC";
		
				$resultadoQueryGrupo = $Conection->query($strQueryGrupo);
				
				if($resultadoQueryGrupo === false) // testa se a query deu certo
				{
					trigger_error('Wrong SQL: ' . $strQueryGrupo . ' Error: ' . $Conection->error, E_USER_ERROR);
				}
				else
				{
					$primeiraVez = true;
					//$usuarios = array();
					
					/*$objGrupo = new stdClass();
					$objGrupo->idGrupo = $idGrupo;
					$objGrupo->usuarios = array();*/
					
					while($linha2 = $resultadoQueryGrupo->fetch_object())
					{
						if($primeiraVez)
						{
							$objGrupo = new stdClass();
							$objGrupo->idGrupo = $idGrupo;
							$objGrupo->usuarios = array();
							$objGrupo->nomeGrupo = utf8_encode($linha2->nome_grupo);
							$objGrupo->descGrupo = utf8_encode($linha2->descricao_grupo);
							
							$primeiraVez = false;
						}
						
						if($linha2->id_usuario == $linha2->fk_adm)
						{
							$usuarioAdm = $linha2->nome;
							$usuarioAdm .= " " . $linha2->sobrenome;
							$objGrupo->admGrupo = utf8_encode($usuarioAdm);
						}
						
						$usuarioGrupo = new stdClass();
						$usuarioGrupo->nomeUsuario = $linha2->nome;
						$usuarioGrupo->nomeUsuario .= " " . $linha2->sobrenome;
						$usuarioGrupo->pontosUsuario = $linha2->pontos;
						
						//array_push($usuarios, $usuarioGrupo);
						array_push($objGrupo->usuarios, $usuarioGrupo);
					}
					//array_push($objGrupo->usuarios, $usuarios);
					
					array_push($grupos, $objGrupo);
				}
			}		
			
			$jsonResposta = json_encode($grupos);
			echo $jsonResposta;
		}
	}
 ?>