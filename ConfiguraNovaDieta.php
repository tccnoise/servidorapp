<?php
	//$strJSON = '{"fk_usuario":11,"dia":"Seg","dieta":[{"nomeRefeicao":"Janta","alimentos":[{"id":1,"nome":"Arroz, integral, cozido","calorias":124,"carboidratos":25.809999465942383,"proteinas":2.5899999141693115,"gordurasTotais":1,"gordurasSaturadas":0.30000001192092896,"gordurasTrans":0,"fibraAlimentar":2.75,"sodio":0.009999999776482582,"porcao":100,"tipoAlimento":"Cereais e derivados"}],"horario":"13:00"},{"nomeRefeicao":"Almoco","alimentos":[{"id":7,"nome":"Biscoito, doce, wafer, recheado de chocolate","calorias":502,"carboidratos":67.54000091552734,"proteinas":5.559999942779541,"gordurasTotais":16.5,"gordurasSaturadas":6.5,"gordurasTrans":7.050000190734863,"fibraAlimentar":1.7999999523162842,"sodio":1.3700000047683716,"porcao":100,"tipoAlimento":"Cereais e derivados"},{"id":8,"nome":"Biscoito, doce, wafer, recheado de morango","calorias":513,"carboidratos":67.3499984741211,"proteinas":4.519999980926514,"gordurasTotais":17.399999618530273,"gordurasSaturadas":6.699999809265137,"gordurasTrans":7.800000190734863,"fibraAlimentar":0.8199999928474426,"sodio":1.2000000476837158,"porcao":100,"tipoAlimento":"Cereais e derivados"}],"horario":"12:30"}]}';
	if(!empty($_POST['dadosDieta']))
	{
	
		$strJSON = $_POST['dadosDieta'];
		
		$strJ = json_decode($strJSON);
		include 'Utilidades.php';
		$deuCerto = false;
		
		$Conection = ConectaBD();
		$i = 0;
		$fk_usuario = $strJ->fk_usuario;
		$dia = $strJ->dia;
		
		$dieta = $strJ->dieta;
		$refeicoes = array();
		$horarios = array();
		$alimentosRefeicoes = array();
		
		for($i = 0; $i < count($dieta); $i++)
		{
			array_push($refeicoes, $dieta[$i]->nomeRefeicao);
			
			$alimentos = array();
			
			for($j = 0; $j < count($dieta[$i]->alimentos); $j++)
			{
				$alimento = new stdClass();
				
				$alimento->id = $dieta[$i]->alimentos[$j]->idAlimento;
				$alimento->porcao = $dieta[$i]->alimentos[$j]->porcao;
				
				array_push($alimentos, $alimento);
			}
			
			array_push($alimentosRefeicoes, $alimentos);
			
			array_push($horarios, $dieta[$i]->horario);
		}		
			
		$strDELETE = "DELETE FROM refeicoes WHERE fk_usuario = '$fk_usuario' AND dia_da_semana = '$dia'";
					
		$resultadoDELETE = $Conection->query($strDELETE);
					
		if($resultadoDELETE === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strDELETE . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			$strINSERT = "INSERT INTO refeicoes (id_refeicao, fk_usuario, fk_alimento, nome_refeicao, dia_da_semana, horario, porcao) VALUES";
			for($i = 0; $i < count($alimentosRefeicoes); $i++)
			{
				for($j = 0; $j < count($alimentosRefeicoes[$i]); $j++)
				{
					$fk_alimento = $alimentosRefeicoes[$i][$j]->id;
					$porcao = $alimentosRefeicoes[$i][$j]->porcao;
					$nomeRefeicao = $refeicoes[$i];
					$horaRefeicao = $horarios[$i];
					
					if($i == 0 && $j == 0)
					{
						$strINSERT .= "('', '$fk_usuario', '$fk_alimento', '$nomeRefeicao', '$dia', '$horaRefeicao', '$porcao')";
					}
					else
					{
						$strINSERT .= ", ('', '$fk_usuario', '$fk_alimento', '$nomeRefeicao', '$dia', '$horaRefeicao', '$porcao')";
					}
				}
				
			}
			$strINSERT .= ";";
			
			$resultadoINSERT = $Conection->query($strINSERT);
						
			if($resultadoINSERT === false) // testa se a query deu certo
			{
				trigger_error('Wrong SQL: ' . $strINSERT . ' Error: ' . $Conection->error, E_USER_ERROR);
			}
			else
			{
				$deuCerto = true;
			}
		}		
		
		$resposta = new stdClass();
		if($deuCerto)
		{
			$resposta->status = 1;
		}
		else
		{
			$resposta->status = 2;
		}
		
		$jsonres = json_encode($resposta);
		echo $jsonres;
			
	}
?>