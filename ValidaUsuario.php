<?php
	include 'Utilidades.php';
	if(!empty($_POST['dadosUsuario']))
	{
		$Conection = ConectaBD();
		
		// Recebendo e decodificando o JSON com dados do usuario
		$strJSON = $_POST['dadosUsuario'];
		$dados = json_decode($strJSON);
		
		// Passando os dados para suas variaveis
		$email = $dados->email;
		$senha = $dados->senha;
		
		// montando a query a ser executada
		$strSELECT = "SELECT * FROM usuarios 
		WHERE email = '$email' AND senha = '$senha'";
		
		// atribuindo o valor da query a uma variavel para auxilio
		$resultadoQuery = $Conection->query($strSELECT);
		
		if($resultadoQuery === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			if($resultadoQuery->num_rows === 1 )
			{
				ProcuraUsuario($email);
			}
			else if($resultadoQuery->num_rows === 0)
			{
				$vetor = new stdClass();
				$vetor->status = 2;
				
				$jsonj = json_encode($vetor);
				echo $jsonj;
			}
			else
			{
				$vetor = new stdClass();
				$vetor->status = 3;
				
				$jsonj = json_encode($vetor);
				echo $jsonj;
			}
		}
		
	}
	else if(!empty($_POST['dadosNovoUsuario']))
	{
		$Conection = ConectaBD();
		
		// Recebendo e decodificando o JSON com dados do usuario
		$strJSON = $_POST['dadosNovoUsuario'];
		$dados = json_decode($strJSON);
		
		// Passando os dados para suas variaveis
		$email = $dados->email;
		$senha = $dados->senha;
		$nome = $dados->nome;
		$sobrenome = $dados->sobrenome;
		
		// montando a query a ser executada
		$strINSERT = "INSERT INTO usuarios (id_usuario, email, senha, nome, sobrenome)
		VALUES ('', '$email', '$senha', '$nome', '$sobrenome')";
		
		if($Conection->query($strINSERT) === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strINSERT . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			$usuariosAdd = $Conection->affected_rows;
			
			ProcuraUsuario($email);
		}
	}
	
	/*function ConectaBD()
	{	
		$hostname = 'localhost';

		$username = 'root';

		$senha = '';

		$banco = 'tccbd';
		
		$Coneccao = new mysqli($hostname, $username, $senha, $banco);
		if ($Coneccao->connect_error) 
		{
			trigger_error('Database connection failed: '  . $Coneccao->connect_error, E_USER_ERROR);
		}
		
		return $Coneccao;
	}*/
	
	function ProcuraUsuario($e)
	{
		$strSELECT = "SELECT * FROM usuarios WHERE email = '$e'";
		
		$Conection2 = ConectaBD();
		$r = $Conection2->query($strSELECT);
		
		if($r === false)
		{
			trigger_error('Wrong SQL: ' . $strSELECT . ' Error: ' . $Conection2->error, E_USER_ERROR);
		}
		else
		{
			if($r->num_rows === 1)
			{
				$linha = $r->fetch_object();
				$vetor = new stdClass();
				$vetor->status = 1;
				$vetor->id = $linha->id_usuario;
				$vetor->email = $linha->email;
				$vetor->senha = $linha->senha;
				$vetor->nome = $linha->nome;
				$vetor->sobrenome = $linha->sobrenome;
				$vetor->fotoPerfil = $linha->foto_perfil;
				$vetor->pontos = $linha->pontos;
				$vetor->bonus = $linha->bonus;
				$vetor->ano = substr($linha->data_pontos,0,4);
				$vetor->mes = substr($linha->data_pontos,5,2);
				$vetor->dia = substr($linha->data_pontos,8,2);
				
				if($linha->b_peso==1)	
				{
					$vetor->peso = true;
					$vetor->pesoAtual = $linha->peso_atual;
					$vetor->pesoObjetivo = $linha->peso_objetivo;
				}
				
				else
				{
					$vetor->peso = false;
					$vetor->pesoAtual = $linha->peso_atual;
					$vetor->pesoObjetivo = $linha->peso_objetivo;
				}
				
				if($linha->b_torax==1)	
				{
					$vetor->torax = true;
					$vetor->toraxAtual = $linha->torax_atual;
					$vetor->toraxObjetivo = $linha->torax_objetivo;
				}
				
				else
				{
					$vetor->torax = false;
					$vetor->toraxAtual = $linha->torax_atual;
					$vetor->toraxObjetivo = $linha->torax_objetivo;
				}
				
				if($linha->b_braco==1)	
				{
					$vetor->braco = true;
					$vetor->bracoAtual = $linha->braco_atual;
					$vetor->bracoObjetivo = $linha->braco_objetivo;
				}
				
				else
				{
					$vetor->braco = false;
					$vetor->bracoAtual = $linha->braco_atual;
					$vetor->bracoObjetivo = $linha->braco_objetivo;
				}
				
				if($linha->b_cintura==1)	
				{
					$vetor->cintura = true;
					$vetor->cinturaAtual = $linha->cintura_atual;
					$vetor->cinturaObjetivo = $linha->cintura_objetivo;
				}
				
				else
				{
					$vetor->cintura = false;
					$vetor->cinturaAtual = $linha->cintura_atual;
					$vetor->cinturaObjetivo = $linha->cintura_objetivo;
				}
				
				if($linha->b_costas==1)	
				{
					$vetor->costas = true;
					$vetor->costasAtual = $linha->costas_atual;
					$vetor->costasObjetivo = $linha->costas_objetivo;
				}
				
				else
				{
					$vetor->costas = false;
					$vetor->costasAtual = $linha->costas_atual;
					$vetor->costasObjetivo = $linha->costas_objetivo;
				}
				
				if($linha->b_coxas==1)	
				{
					$vetor->coxas = true;
					$vetor->coxasAtual = $linha->coxas_atual;
					$vetor->coxasObjetivo = $linha->coxas_objetivo;
				}
				
				else
				{
					$vetor->coxas = false;
					$vetor->coxasAtual = $linha->coxas_atual;
					$vetor->coxasObjetivo = $linha->coxas_objetivo;
				}
				
				if($linha->b_panturrilha==1)	
				{
					$vetor->panturrilha = true;
					$vetor->panturrilhaAtual = $linha->panturrilha_atual;
					$vetor->panturrilhaObjetivo = $linha->panturrilha_objetivo;
				}
				
				else
				{
					$vetor->panturrilha = false;
					$vetor->panturrilhaAtual = $linha->panturrilha_atual;
					$vetor->panturrilhaObjetivo = $linha->panturrilha_objetivo;
				}
				
				
				$jsonj = json_encode($vetor);
				echo $jsonj;
			}
			else if($r->num_rows === 0)
			{
				$vetor = new stdClass();
				$vetor->status = 2;
				
				$jsonj = json_encode($vetor);
				echo $jsonj;
			}
			else
			{
				$vetor = new stdClass();
				$vetor->status = 3;
				
				$jsonj = json_encode($vetor);
				echo $jsonj;
			}
		}
	}
?>