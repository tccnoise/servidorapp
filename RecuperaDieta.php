 <?php
	if(!empty($_POST['DietaUsuario']))
	{
		include 'Utilidades.php';
		
		$strJSON = $_POST['DietaUsuario'];
		//$strJSON = '{"id":"11"}';
		$dado = json_decode($strJSON);
		
		$usuarioID = $dado->id;
		
		$dias = new stdClass();
		$dias->Seg = array();
		$dias->Ter = array();
		$dias->Qua = array();
		$dias->Qui = array();
		$dias->Sex = array();
		$dias->Sab = array();
		$dias->Dom = array();
		
		$Conection = ConectaBD();
				
		$strQuery = "SELECT * FROM alimentos INNER JOIN refeicoes ON alimentos.id_alimento = refeicoes.fk_alimento WHERE fk_usuario = '$usuarioID' ";
		
		$resultadoQuery = $Conection->query($strQuery);
					
		if($resultadoQuery === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strQuery . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			while($linha = $resultadoQuery->fetch_object())
			{
				$objDieta = new stdClass();
				
				$objDieta->dia = $linha->dia_da_semana;
				$objDieta->idAlimento = $linha->id_alimento;
				$objDieta->nomeAlimento = $linha->nome_alimento;
				$objDieta->porcao = $linha->porcao;
				$objDieta->calorias = $linha->calorias;
				$objDieta->carboidratos = $linha->carboidratos;
				$objDieta->proteinas = $linha->proteinas;
				$objDieta->gordurasTotais = $linha->gorduras_totais;
				$objDieta->gordurasSaturadas = $linha->gorduras_saturadas;
				$objDieta->gordurasTrans = $linha->gorduras_trans;
				$objDieta->fibraAlimentar = $linha->fibra_alimentar;
				$objDieta->sodio = $linha->sodio;
				$objDieta->tipoAlimento = $linha->tipo_alimento;
				$objDieta->nomeRefeicao = $linha->nome_refeicao;
				$objDieta->horario = $linha->horario;
				
				if($linha->dia_da_semana == 'Seg')
				{					
					array_push($dias->Seg, $objDieta);
				}
				else if($linha->dia_da_semana == 'Ter')
				{
					array_push($dias->Ter, $objDieta);
				}
				else if($linha->dia_da_semana == 'Qua')
				{
					array_push($dias->Qua, $objDieta);
				}
				else if($linha->dia_da_semana == 'Qui')
				{
					array_push($dias->Qui, $objDieta);
				}
				else if($linha->dia_da_semana == 'Sex')
				{
					array_push($dias->Sex, $objDieta);
				}
				else if($linha->dia_da_semana == 'Sab')
				{
					array_push($dias->Sab, $objDieta);
				}
				else if($linha->dia_da_semana == 'Dom')
				{
					array_push($dias->Dom, $objDieta);
				}
			}
			
			$dieta = new stdClass();
			$dieta->Seg = new stdClass();
			$dieta->Seg->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Seg); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Seg[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Seg->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Seg[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Seg[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Seg[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Seg[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Seg[$i]->nomeAlimento);
				$alimento->porcao = $dias->Seg[$i]->porcao;
				$alimento->calorias = $dias->Seg[$i]->calorias;
				$alimento->carboidratos = $dias->Seg[$i]->carboidratos;
				$alimento->proteinas = $dias->Seg[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Seg[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Seg[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Seg[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Seg[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Seg[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Seg[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Seg) - 1)
				{
					array_push($dieta->Seg->refeicoes, $refeicao);
				}
			}
			
			$dieta->Ter = new stdClass();
			$dieta->Ter->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Ter); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Ter[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Ter->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Ter[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Ter[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Ter[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Ter[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Ter[$i]->nomeAlimento);
				$alimento->porcao = $dias->Ter[$i]->porcao;
				$alimento->calorias = $dias->Ter[$i]->calorias;
				$alimento->carboidratos = $dias->Ter[$i]->carboidratos;
				$alimento->proteinas = $dias->Ter[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Ter[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Ter[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Ter[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Ter[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Ter[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Ter[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Ter) - 1)
				{
					array_push($dieta->Ter->refeicoes, $refeicao);
				}
			}
			
			$dieta->Qua = new stdClass();
			$dieta->Qua->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Qua); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Qua[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Qua->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Qua[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Qua[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Qua[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Qua[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Qua[$i]->nomeAlimento);
				$alimento->porcao = $dias->Qua[$i]->porcao;
				$alimento->calorias = $dias->Qua[$i]->calorias;
				$alimento->carboidratos = $dias->Qua[$i]->carboidratos;
				$alimento->proteinas = $dias->Qua[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Qua[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Qua[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Qua[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Qua[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Qua[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Qua[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Qua) - 1)
				{
					array_push($dieta->Qua->refeicoes, $refeicao);
				}
			}
					
			
			$dieta->Qui = new stdClass();
			$dieta->Qui->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Qui); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Qui[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Qui->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Qui[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Qui[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Qui[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Qui[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Qui[$i]->nomeAlimento);
				$alimento->porcao = $dias->Qui[$i]->porcao;
				$alimento->calorias = $dias->Qui[$i]->calorias;
				$alimento->carboidratos = $dias->Qui[$i]->carboidratos;
				$alimento->proteinas = $dias->Qui[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Qui[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Qui[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Qui[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Qui[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Qui[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Qui[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Qui) - 1)
				{
					array_push($dieta->Qui->refeicoes, $refeicao);
				}
			}
			
			
			$dieta->Sex = new stdClass();
			$dieta->Sex->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Sex); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Sex[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Sex->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Sex[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Sex[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Sex[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Sex[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Sex[$i]->nomeAlimento);
				$alimento->porcao = $dias->Sex[$i]->porcao;
				$alimento->calorias = $dias->Sex[$i]->calorias;
				$alimento->carboidratos = $dias->Sex[$i]->carboidratos;
				$alimento->proteinas = $dias->Sex[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Sex[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Sex[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Sex[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Sex[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Sex[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Sex[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Sex) - 1)
				{
					array_push($dieta->Sex->refeicoes, $refeicao);
				}
			}
			
			
			$dieta->Sab = new stdClass();
			$dieta->Sab->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Sab); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Sab[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Sab->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Sab[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Sab[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Sab[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Sab[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Sab[$i]->nomeAlimento);
				$alimento->porcao = $dias->Sab[$i]->porcao;
				$alimento->calorias = $dias->Sab[$i]->calorias;
				$alimento->carboidratos = $dias->Sab[$i]->carboidratos;
				$alimento->proteinas = $dias->Sab[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Sab[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Sab[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Sab[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Sab[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Sab[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Sab[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Sab) - 1)
				{
					array_push($dieta->Sab->refeicoes, $refeicao);
				}
			}
			
			$dieta->Dom = new stdClass();
			$dieta->Dom->refeicoes = array();
			$refeicao = new stdClass();
			$refeicao->nomeRefeicao = "nomeInvalido";
			$refeicao->alimentos = array();
			
			for($i = 0; $i < count($dias->Dom); $i++)
			{
				
				if($refeicao->nomeRefeicao != $dias->Dom[$i]->nomeRefeicao)
				{
					if($i != 0)
					{
						//$nomeRefeicaoAux = $refeicao->nomeRefeicao;
						array_push($dieta->Dom->refeicoes, $refeicao);
						
						$refeicao = new stdClass();
						$refeicao->nomeRefeicao = $dias->Dom[$i]->nomeRefeicao;
						$refeicao->alimentos = array();
					}
					
					$refeicao->nomeRefeicao = $dias->Dom[$i]->nomeRefeicao;
					$refeicao->horario = $dias->Dom[$i]->horario;
				}
				
				$alimento = new stdClass();
				$alimento->idAlimento = $dias->Dom[$i]->idAlimento;
				$alimento->nomeAlimento = utf8_encode($dias->Dom[$i]->nomeAlimento);
				$alimento->porcao = $dias->Dom[$i]->porcao;
				$alimento->calorias = $dias->Dom[$i]->calorias;
				$alimento->carboidratos = $dias->Dom[$i]->carboidratos;
				$alimento->proteinas = $dias->Dom[$i]->proteinas;
				$alimento->gordurasTotais = $dias->Dom[$i]->gordurasTotais;
				$alimento->gordurasSaturadas = $dias->Dom[$i]->gordurasSaturadas;
				$alimento->gordurasTrans = $dias->Dom[$i]->gordurasTrans;
				$alimento->fibraAlimentar = $dias->Dom[$i]->fibraAlimentar;
				$alimento->sodio = $dias->Dom[$i]->sodio;
				$alimento->tipoAlimento = utf8_encode($dias->Dom[$i]->tipoAlimento);
				
				
				array_push($refeicao->alimentos, $alimento);
					
				if($i == count($dias->Dom) - 1)
				{
					array_push($dieta->Dom->refeicoes, $refeicao);
				}
			}
			
			
			
			$jsonResposta = json_encode($dieta);
			echo $jsonResposta;
		}
	}
 ?>