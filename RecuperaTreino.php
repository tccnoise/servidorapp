 <?php
	if(!empty($_POST['TreinoUsuario']))
	{
		include 'Utilidades.php';
		
		$strJSON = $_POST['TreinoUsuario'];
		//$strJSON = '{"id":"11"}';
		$dado = json_decode($strJSON);
		
		$usuarioID = $dado->id;
		
		$dias = new stdClass();
		$dias->Seg = array();
		$dias->Ter = array();
		$dias->Qua = array();
		$dias->Qui = array();
		$dias->Sex = array();
		$dias->Sab = array();
		$dias->Dom = array();
		
		$Conection = ConectaBD();
				
		$strQuery = "SELECT nome_exercicio, tipo2, series, repeticoes, peso, tempo_execucao, tempo_descanso, dia_da_semana FROM treinos INNER JOIN exercicios ON exercicios.id_exercicio = treinos.fk_exercicio WHERE fk_usuario = '$usuarioID' ";
					
		$resultadoQuery = $Conection->query($strQuery);
					
		if($resultadoQuery === false) // testa se a query deu certo
		{
			trigger_error('Wrong SQL: ' . $strQuery . ' Error: ' . $Conection->error, E_USER_ERROR);
		}
		else
		{
			while($linha = $resultadoQuery->fetch_row())
			{
				$objTreino = new stdClass();
				
				$objTreino->nome = $linha[0];
				$objTreino->tipo2 = $linha[1];
				$objTreino->series = $linha[2];
				$objTreino->repeticoes = $linha[3];
				$objTreino->peso = $linha[4];
				$objTreino->tempoExecucao = $linha[5];
				$objTreino->tempoDescanco = $linha[6];
				
				if($linha[7] == 'Seg')
				{					
					array_push($dias->Seg, $objTreino);
				}
				else if($linha[7] == 'Ter')
				{
					array_push($dias->Ter, $objTreino);
				}
				else if($linha[7] == 'Qua')
				{
					array_push($dias->Qua, $objTreino);
				}
				else if($linha[7] == 'Qui')
				{
					array_push($dias->Qui, $objTreino);
				}
				else if($linha[7] == 'Sex')
				{
					array_push($dias->Sex, $objTreino);
				}
				else if($linha[7] == 'Sab')
				{
					array_push($dias->Sab, $objTreino);
				}
				else if($linha[7] == 'Dom')
				{
					array_push($dias->Dom, $objTreino);
				}
			}
			
			$jsonResposta = json_encode($dias);
			echo $jsonResposta;
		}
	}
 ?>